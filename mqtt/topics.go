package mqtt

// TopicInfo contains information about a topic
type TopicInfo struct {
	Topic        string
	Subscription string
	Qos          byte
}

const qos byte = 2

// RoutesSubscription is the subscription topic name for route-shaper
const RoutesSubscription string = "$share/syfer/routes"

// RoutesTopic is the shared topic name for route-shaper
const RoutesTopic string = "routes"

// Topics dictionary contains all topics
var Topics = map[string]*TopicInfo{
	RoutesSubscription: &TopicInfo{
		Topic:        RoutesTopic,
		Qos:          qos,
		Subscription: RoutesSubscription,
	},
}
