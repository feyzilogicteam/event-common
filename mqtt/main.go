package mqtt

import (
	"fmt"
	"log"
	"os"
	"time"

	"github.com/golang/glog"

	mqtt "github.com/eclipse/paho.mqtt.golang"
)

// ConnectionInfo contains all necessary info to connect
type ConnectionInfo struct {
	Broker                string
	ClientID              string
	Logger                *log.Logger
	OnConnect             mqtt.OnConnectHandler
	ConnectionLostHandler mqtt.ConnectionLostHandler
	CredentialsProvider   mqtt.CredentialsProvider
}

// Connect to the MQTT Broker
func (info *ConnectionInfo) Connect() (mqtt.Client, error) {
	err := validate(info)
	if err != nil {
		return nil, err
	}

	mqtt.ERROR = info.Logger
	opts := mqtt.NewClientOptions().
		AddBroker(info.Broker).
		SetClientID(info.ClientID).
		SetKeepAlive(60 * time.Second).
		SetPingTimeout(10 * time.Second).
		SetOrderMatters(true).
		SetCredentialsProvider(info.CredentialsProvider).
		SetOnConnectHandler(info.OnConnect)

	if info.ConnectionLostHandler != nil {
		opts.SetConnectionLostHandler(info.ConnectionLostHandler)
	}

	client := mqtt.NewClient(opts)
	token := client.Connect()
	token.Wait()
	if err = token.Error(); err != nil {
		return nil, err
	}

	fmt.Printf("Connected to server\n")
	return client, nil
}

func validate(info *ConnectionInfo) error {
	if info.ClientID == "" {
		return fmt.Errorf("Missing 'ClientID'")
	}

	if info.Broker == "" {
		return fmt.Errorf("Missing 'Broker'")
	}

	if info.Logger == nil {
		glog.Warning("Missing 'Logger' so setting it a default logger")
		info.Logger = log.New(os.Stdout, "", 0)
	}

	if info.CredentialsProvider == nil {
		return fmt.Errorf("Missing 'CredentialProvider'")
	}

	return nil
}
