NAME=event-common
VERSION=0.0.1

export GO111MODULE=on

.PHONY: build
build: get
	@go build -o $(NAME)

.PHONY: run
run: build
	@./$(NAME) -stderrthreshold INFO

.PHONY: clean
clean:
	@rm -f $(NAME)

.PHONY: test
test: get
	@go test -v ./test/*_test.go

.PHONY: get
get:
	 @go get -v -d