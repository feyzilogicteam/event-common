package test

import (
	"os"
	"testing"

	auth "bitbucket.org/feyzilogicteam/event-common/auth"
	"github.com/stretchr/testify/assert"
	"golang.org/x/oauth2/clientcredentials"
)

func TestToken(t *testing.T) {
	t.Skip("Manual test that requires Client ID + Secret")

	c := getCredentials()

	token, err := auth.Token(c)

	assert.Nilf(t, err, "An error occured during authentication with error '%v'", err)
	assert.NotNil(t, token.AccessToken, "Missing Auth AccessToken")
	assert.Equal(t, token.TokenType, "Bearer")
}

func TestRefreshToken(t *testing.T) {
	t.Skip("Manual test that requires Client ID + Secret")

	c := getCredentials()

	token, err := auth.RefreshToken(c)

	assert.Nilf(t, err, "An error occured during refresh with error '%v'", err)
	assert.NotNil(t, token.RefreshToken, "Missing Auth AccessToken")
	assert.Equal(t, token.TokenType, "Bearer")
}

func getCredentials() clientcredentials.Config {
	c := clientcredentials.Config{
		ClientID:     os.Getenv("client_id"),
		ClientSecret: os.Getenv("client_secret"),
	}

	return c
}
