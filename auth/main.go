package auth

import (
	"context"
	"fmt"
	"net/url"

	"golang.org/x/oauth2"
	"golang.org/x/oauth2/clientcredentials"
)

const audience string = "https://www.mysyfer.com"
const authEndpoint string = "https://feyzilogic.auth0.com/oauth/token"

// Token from Auth0 using grant type client_credentials
func Token(c clientcredentials.Config) (*oauth2.Token, error) {
	conf, err := validate(&c)
	if err != nil {
		return nil, err
	}

	ctx := context.Background()
	token, err := conf.Token(ctx)

	return token, err
}

// RefreshToken will refresh the token from Auth0
func RefreshToken(c clientcredentials.Config) (*oauth2.Token, error) {
	conf, err := validate(&c)
	if err != nil {
		return nil, err
	}

	ctx := context.Background()
	tokenSource := conf.TokenSource(ctx)

	return tokenSource.Token()
}

func validate(config *clientcredentials.Config) (*clientcredentials.Config, error) {
	if config.ClientID == "" {
		return nil, fmt.Errorf("Client ID is not set")
	}

	if config.ClientSecret == "" {
		return nil, fmt.Errorf("Client Secret is not set")
	}

	if config.TokenURL == "" {
		config.TokenURL = authEndpoint
	}

	if config.EndpointParams == nil {
		config.EndpointParams = url.Values{"audience": []string{audience}}
	} else if config.EndpointParams.Get("audience") == "" {
		config.EndpointParams.Set("audience", audience)
	}

	return config, nil
}
