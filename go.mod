module bitbucket.org/feyzilogicteam/event-common

require (
	github.com/eclipse/paho.mqtt.golang v1.1.1
	github.com/golang/glog v0.0.0-20160126235308-23def4e6c14b
	github.com/stretchr/testify v1.3.0
	golang.org/x/oauth2 v0.0.0-20190115181402-5dab4167f31c
)
